﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace resource_allocation
{
    /// <summary>
    /// One-dimensional bin packing with first-fit, first-fit decreasing and best-fit algorithms for variable bin sizes. 
    /// Comment out the sorting for first-fit only. 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {            
            Console.WriteLine("Bin packing with first-fit");
            BinPackingWithFirstFit();

            Console.WriteLine("Bin packing with first-fit decreasing");
            BinPackingWithFirstFitDecreasing();

            Console.WriteLine("Bin packing with best-fit");
            BinPackingWithBestFit();

            Console.ReadKey(); 
        }

        private static void BinPackingWithBestFit()
        {
            List<double> buys = new List<double>()
            {
                600,
                700,
                800
            };

            List<double> sales = new List<double>()
            {
                500,
                1000,
                600
            };

            double largestBuy = buys.Max();
            double largestSale = sales.Max();

            List<double> bins, items;

            if (largestBuy > largestSale)
            {
                bins = buys;
                items = sales;
            }
            else
            {
                bins = sales;
                items = buys;
            }

            items.Sort((a, b) => b.CompareTo(a));
            
            double[,] allocation = new double[bins.Count, items.Count];

            for (int i = 0; i < items.Count; i++)
            {
                while (items[i] != 0)
                {
                    double max = bins.Max(); 
                    int maxIndex = bins.IndexOf(max);

                    double diff = items[i] - bins[maxIndex]; 
                    if(diff <= 0)
                    {
                        bins[maxIndex] -= items[i];
                        allocation[i, maxIndex] = items[i];
                        items[i] = 0; 
                    }
                    else
                    {
                        items[i] -= bins[maxIndex];
                        allocation[i, maxIndex] = bins[maxIndex];
                        bins[maxIndex] = 0;
                    }
                }
            }

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < bins.Count; j++)
                {
                    Console.Write(allocation[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }

        private static void BinPackingWithFirstFitDecreasing()
        {
            List<double> buys = new List<double>()
            {
                600,
                700,
                800
            };

            List<double> sales = new List<double>()
            {
                500,
                1000,
                600
            };

            double largestBuy = buys.Max();
            double largestSale = sales.Max();

            buys.Sort((a, b) => b.CompareTo(a));
            sales.Sort((a, b) => b.CompareTo(a));

            List<double> bins, items;

            if (largestBuy > largestSale)
            {
                bins = buys;
                items = sales;
            }
            else
            {
                bins = sales;
                items = buys;
            }
            
            double[,] allocation = new double[bins.Count, items.Count];

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < bins.Count; j++)
                {
                    if (bins[j] >= items[i])
                    {
                        bins[j] -= items[i];
                        allocation[i, j] = items[i];
                        items[i] = 0;
                    }
                    else
                    {
                        items[i] -= bins[j];
                        allocation[i, j] = bins[j];                        
                        bins[j] = 0;
                    }
                }
            }

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < bins.Count; j++)
                {
                    Console.Write(allocation[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        private static void BinPackingWithFirstFit()
        {
            List<double> buys = new List<double>()
            {
                600,
                700,
                800
            };

            List<double> sales = new List<double>()
            {
                500,
                1000,
                600
            };

            double largestBuy = buys.Max();
            double largestSale = sales.Max();
            
            List<double> bins, items;

            if (largestBuy > largestSale)
            {
                bins = buys;
                items = sales;
            }
            else
            {
                bins = sales;
                items = buys;
            }

            double[,] allocation = new double[bins.Count, items.Count];

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < bins.Count; j++)
                {
                    if (bins[j] >= items[i])
                    {
                        bins[j] -= items[i];
                        allocation[i, j] = items[i];
                        items[i] = 0;
                    }
                    else
                    {
                        items[i] -= bins[j];
                        allocation[i, j] = bins[j];                        
                        bins[j] = 0;
                    }
                }
            }

            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < bins.Count; j++)
                {
                    Console.Write(allocation[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
